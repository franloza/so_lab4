/*
 *  chardev_leds.c: Creates a write-only char device that switch on the 
	leds of the keyboard
 */

#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/fs.h>
#include <asm/uaccess.h>	/* for copy_to_user */
#include <linux/cdev.h>
#include <asm-generic/errno.h>
#include <linux/init.h>
#include <linux/tty.h>      /* For fg_console */
#include <linux/kd.h>       /* For KDSETLED */
#include <linux/vt_kern.h>
#include <linux/version.h> /* For LINUX_VERSION_CODE */


MODULE_LICENSE("GPL");

/*
 *  Prototypes
 */
int init_module(void);
void cleanup_module(void);
static int device_open(struct inode *, struct file *);
static int device_release(struct inode *, struct file *);
static ssize_t device_read(struct file *, char *, size_t, loff_t *);
static ssize_t device_write(struct file *, const char *, size_t, loff_t *);
struct tty_driver* get_kbd_driver_handler(void);
static inline int set_leds(struct tty_driver* handler, unsigned int mask);

#define SUCCESS 0
#define DEVICE_NAME "leds" /* Dev name as it appears in /proc/devices   */
#define BUF_LEN 80		/* Max length of the message from the device */
#define ALL_LEDS_ON 0x7
#define ALL_LEDS_OFF 0
#define LED_1_ON 0x1
#define LED_2_ON 0x2
#define LED_3_ON 0x4



/*
 * Global variables are declared as static, so are global within the file.
 */
struct tty_driver* kbd_driver= NULL;
dev_t start;
struct cdev* chardev=NULL;
static int Device_Open = 0;	/* Is device open?
				 * Used to prevent multiple access to device */
static char mask_buf[BUF_LEN];	/*Buffer to store the mask*/

static struct file_operations fops = {
    .read = device_read,
    .write = device_write,
    .open = device_open,
    .release = device_release
};

/*
 * This function is called when the module is loaded
 */
int init_module(void)
{
    int major;		/* Major number assigned to our device driver */
    int minor;		/* Minor number assigned to the associated character device */

    /* Get available (major,minor) range */
    if (alloc_chrdev_region (&start, 0, 1,DEVICE_NAME)) {
        printk(KERN_INFO "Can't allocate chrdev_region()");
        return -ENOMEM;
    }

    /* Create associated cdev */
    if ((chardev=cdev_alloc())==NULL) {
        printk(KERN_INFO "cdev_alloc() failed ");
        return -ENOMEM;
    }

    cdev_init(chardev,&fops);

    if (cdev_add(chardev,start,1)) {
        printk(KERN_INFO "cdev_add() failed ");
        return -ENOMEM;
    }

    major=MAJOR(start);
    minor=MINOR(start);

    //Get the driver for the keyboard
    kbd_driver= get_kbd_driver_handler();

    printk(KERN_INFO "I was assigned major number %d. To talk to\n", major);
    printk(KERN_INFO "the driver, create a dev file with\n");
    printk(KERN_INFO "'sudo mknod -m 666 /dev/%s c %d %d'.\n", DEVICE_NAME, major,minor);
    printk(KERN_INFO "Write the mask to switch on/off the keyboard leds.\n");
    printk(KERN_INFO "Remove the device file and module when done.\n");

    return SUCCESS;
}

/*
 * This function is called when the module is unloaded
 */
void cleanup_module(void)
{
   //Switch off all the leds
   set_leds(kbd_driver,ALL_LEDS_OFF);
   /* Destroy chardev */
    if (chardev)
        cdev_del(chardev);

    /*
     * Unregister the device
     */
    unregister_chrdev_region(start, 1);
}

/*
 * Called when a process tries to open the device file, like
 * "cat /dev/leds"
 */
static int device_open(struct inode *inode, struct file *file)
{
	if (Device_Open)
        	return -EBUSY;
	Device_Open++;
	/* Increase the module's reference counter */
	try_module_get(THIS_MODULE);

   	return SUCCESS;

}

/*
 * Called when a process closes the device file.
 */
static int device_release(struct inode *inode, struct file *file)
{
	Device_Open--;              /* We're now ready for our next caller */

    /*
     * Decrement the usage count, or else once you opened the file, you'll
     * never get get rid of the module.
     */
    module_put(THIS_MODULE);

    return 0;

}

/*
 * Called when a process, which already opened the dev file, attempts to
 * read from it.
 */
static ssize_t device_read(struct file *filp,	/* see include/linux/fs.h   */
                           char *buffer,	/* buffer to fill with data */
                           size_t length,	/* length of the buffer     */
                           loff_t * offset)
{
    printk(KERN_ALERT "Sorry, this operation isn't supported.\n");
    return -EPERM;
}

/*
 * Called when a process writes to dev file: echo 2 > /dev/leds
 */
static ssize_t
device_write(struct file *filp, const char *buff, size_t len, loff_t * off)
{
	int bytes_to_write = len;
	if (copy_from_user(mask_buf,buff,bytes_to_write))
	        return -EFAULT;

	printk(KERN_INFO "Buffer that contains the mask: %s", mask_buf);

	char led1, led2, led3;
	led1 = led2 = led3 = 0;

	int i =0;
	while (mask_buf[i] != '\n' && i < 3) {
		if(mask_buf[i] - '0' == 1) led1 = 1;
		else if(mask_buf[i] - '0' == 2) led2 = 1;
		else if(mask_buf[i] - '0' == 3) led3 = 1;
		i++;
	}

	printk(KERN_INFO "Led 1: %s\n", (led1)?"on":"off");
	printk(KERN_INFO "Led 2: %s\n", (led2)?"on":"off");
	printk(KERN_INFO "Led 3: %s\n", (led3)?"on":"off");

	unsigned int mask = 0x0;
	if(led1) mask = LED_2_ON | mask;
        if(led2) mask = LED_3_ON | mask;
        if(led3) mask = LED_1_ON | mask;
	set_leds(kbd_driver,mask);

	return bytes_to_write;
}

/* Get driver handler */
struct tty_driver* get_kbd_driver_handler(void)
{
    printk(KERN_INFO "modleds: loading\n");
    printk(KERN_INFO "modleds: fgconsole is %x\n", fg_console);
#if ( LINUX_VERSION_CODE > KERNEL_VERSION(2,6,32) )
    return vc_cons[fg_console].d->port.tty->driver;
#else
    return vc_cons[fg_console].d->vc_tty->driver;
#endif
}

/* Set led state to that specified by mask */
static inline int set_leds(struct tty_driver* handler, unsigned int mask)
{
#if ( LINUX_VERSION_CODE > KERNEL_VERSION(2,6,32) )
    return (handler->ops->ioctl) (vc_cons[fg_console].d->port.tty, KDSETLED,mask);
#else
    return (handler->ops->ioctl) (vc_cons[fg_console].d->vc_tty, NULL, KDSETLED, mask);
#endif
}

