/*COMPILE WITH: gcc -Wall -g leds_user.c -o leds_user*/
#include <stdlib.h>
#include <string.h>
#include <getopt.h>
#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>

#define USAGE		"Usage: %s -m <mask> -b <binary number[0-7])>\n"
#define EXAMPLE		"Example:\n%s -b 5 (Will turn leds 1 and 3 in the keyboard)\n"
#define EXAMPLE2 	"Example:\n%s -m 13 (Will turn on leds 1 and 3 in the keyboard)\n"
#define LED_FILE 	"/dev/leds"


int turnLeds(char* mask);
int binary(int decNum);

int main(int argc, char **argv) {
	int res = 0; // Resulting code of the functions call

	int opt;
	int binMode=0,maskMode=0;
	int decNum = -1;
	char* mask=NULL;
	while((opt = getopt(argc, argv, "b:m:")) != -1) {
		switch(opt) {
			case 'b':
				decNum = atoi(optarg);
				binMode = 1;
				break;
			case 'm':
				mask = optarg;
				maskMode = 1;
				break;
			default: /* '?' */
				fprintf(stderr, USAGE, argv[0]);
				fprintf(stderr, EXAMPLE, argv[0]);
				exit(-1);
		}
	}

	if (binMode) {
		// Any parameter missing?
		if( decNum < 0 || decNum > 7) {
			fprintf(stderr, USAGE, argv[0]);
			fprintf(stderr, EXAMPLE, argv[0]);
			exit(-1);
		}

		// Execute Binary Mode
		fprintf(stdout, "Showing binary number of %i in the keyboard leds...", decNum);
		res = binary(decNum);

	} else if (maskMode) {
		// Any parameter missing?
		if(mask == NULL) {
			fprintf(stderr, USAGE, argv[0]);
			fprintf(stderr, EXAMPLE2, argv[0]);
			exit(-1);
		}
		// Execute Mask Mode
		fprintf(stdout, "Applying mask %s to the keyboard leds...", mask);
		res = turnLeds(mask);
	}
	else {
		fprintf(stderr, "Please, enter a mode\n");
		exit(-1);
	}

	if(res < 0) {
		fprintf(stderr, "The format of the parameter isn't right\n");
		fprintf(stderr, USAGE, argv[0]);
		exit(-1);
	}
	fprintf(stdout, "Done\n");
	
	return(0);
}

int turnLeds(char* mask) {
	int fd,res = 1;

	if((fd = open(LED_FILE, O_WRONLY)) < 0) {
		return -ENOENT;
	}

	/*Reset the leds*/
	if((res = write(fd,mask,strlen("000"))) < 0){
		return res;
	}
	
	if((res = write(fd,mask,strlen(mask))) < 0){
		return res;
	}

	if((res = close(fd)) < 0)
		return res;
	return 1;
}

int binary(int decNum) {
	char* mask = NULL;
	switch(decNum) {
			case 0: mask = "000"; break;
                        case 1: mask = "03"; break;
                        case 2: mask = "2"; break;
                        case 3: mask = "23"; break;
                        case 4: mask = "1"; break;
                        case 5: mask = "13"; break;
                        case 6: mask = "12"; break;
                        case 7: mask = "123"; break;
                        default: mask = "000";
                }
	return turnLeds(mask);

}
